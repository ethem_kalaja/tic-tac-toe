T = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
vlera = True

class vendiZene(Exception):
    pass


def fushaPrint():  # printon fushen

    for n in T:
        for m in n:
            print(m, end='     ')
        print()
        print()


def inputX():  # mer kordinatat e vendosjes se x nga lojtari X
    while True:

        try:
            a = int(input('Jepni koordianten e X: '))
            if a < 1 or a > 9:
                raise ValueError

            n = a % 3
            n -= 1
            m = a // 3
            if n == -1:
                n = 2
                m -= 1
            if T[m][n] == 'O' or T[m][n] == 'X':
                raise vendiZene
            T[m][n] = 'X'
            m = 0
            n = 0
            fushaPrint()

            break
        except ValueError:
            print('Ju lutem jepni nje numer nga 1 tek 9.')

        except vendiZene:
            print('Ju lutem jepni nje koordinate tjeter sepse kjo koordinate eshte e zene.')


def inputY():  # mer kordinaten e vendosjes se O nga lojtari O
    while True:
        try:
            a = int(input('Jepni koordianten e O: '))
            if a < 1 or a > 9:
                raise ValueError
            n = a % 3
            n -= 1
            m = a // 3
            if n == -1:
                n = 2
                m -= 1
            if T[m][n] == 'O' or T[m][n] == 'X':
                raise vendiZene
            T[m][n] = 'O'
            m = 0
            n = 0
            fushaPrint()

            break
        except ValueError:
            print('Ju lutem jepni nje numer nga 1 tek 9.')

        except vendiZene:
            print('Ju lutem jepni nje koordinate tjeter sepse kjo koordinate eshte e zene.')


def gjejFituesin():  # gjen nese ka apo jo fitues pasi lojtari jep kordinaten
    if T[0][1] == T[0][2] == T[0][0] or \
            T[1][0] == T[1][1] == T[1][2] or \
            T[2][0] == T[2][1] == T[2][2] or \
            T[0][0] == T[1][0] == T[2][0] or \
            T[0][1] == T[1][1] == T[2][1] or \
            T[0][2] == T[1][2] == T[2][2] or \
            T[0][0] == T[1][1] == T[2][2] or \
            T[0][2] == T[1][1] == T[2][0]:
        return True
    else:
        return False


fushaPrint()

while True:
    if vlera:
        inputX()
    else:
        inputY()
    if vlera == True:
        vlera = False
    else:
        vlera = True

    if gjejFituesin():

        if vlera == True:
            print('Fituesi eshte lojtari O')
            break
        else:
            print('Fituesi eshte lojtari X')
            break

